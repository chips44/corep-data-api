<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Objet;

/**
 * Load data for entity "Objet" 
 * 
 * @category DataFixture
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 */
class ObjetFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * Get dependencies
     * 
     * @return array
     */
    public function getDependencies()
    {
        return array(
            PoleFixture::class,
            ParticipantFixture::class,
        );
    }
    
    /**
     * Load data
     * 
     * @param ObjectManager $manager Object manager
     * 
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        // @var Objet
        $objet1 = new Objet();
        $objet1->setNom        ('Vélo');
        $objet1->setPole       ($this->getReference('pole-mecanique'));
        $objet1->setAccueillant($this->getReference('participant-accueil'));
        $objet1->setBricoleur  ($this->getReference('participant-bricole'));
        $objet1->setVisiteur   ($this->getReference('participant-visiteur1'));
        $manager->persist($objet1);
        
        // @var Objet
        $objet2 = new Objet();
        $objet2->setNom        ('Manteau');
        $objet2->setPole       ($this->getReference('pole-couture'));
        $objet2->setAccueillant($this->getReference('participant-accueil'));
        $objet2->setBricoleur  ($this->getReference('participant-bricole'));
        $objet2->setVisiteur   ($this->getReference('participant-visiteur1'));
        $manager->persist($objet2);
        
        // @var Objet
        $objet3 = new Objet();
        $objet3->setNom        ('Aspirateur');
        $objet3->setPole       ($this->getReference('pole-electro-menager'));
        $objet3->setAccueillant($this->getReference('participant-accueil'));
        $objet3->setBricoleur  ($this->getReference('participant-bricole'));
        $objet3->setVisiteur   ($this->getReference('participant-visiteur1'));
        $manager->persist($objet3);
        
        // @var Objet
        $objet4 = new Objet();
        $objet4->setNom        ('Ordinateur');
        $objet4->setPole       ($this->getReference('pole-informatique'));
        $objet4->setAccueillant($this->getReference('participant-accueil'));
        $objet4->setBricoleur  ($this->getReference('participant-bricole'));
        $objet4->setVisiteur   ($this->getReference('participant-visiteur2'));
        $manager->persist($objet4);
        
        $manager->flush();
        
        $this->addReference('objet-velo'      , $objet1);
        $this->addReference('objet-pantalon'  , $objet2);
        $this->addReference('objet-aspirateur', $objet3);
        $this->addReference('objet-ordinateur', $objet4);

    }
}
