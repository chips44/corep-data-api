<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Participant;

/**
 * Load data for entity "Participant"
 * 
 * @category DataFixture
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 */
class ParticipantFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * Get dependencies
     * 
     * @return array
     */
    public function getDependencies()
    {
        return array(
            AtelierFixture::class,
            UserFixture::class
        );
    }
    
    /**
     * Load data
     * 
     * @param ObjectManager $manager Object manager
     * 
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        // @var Participant
        $participant1 = new Participant();
        $participant1->setPseudo('Marie');
        $participant1->setEmail('marie@cooreparation-nantes.fr');
        $participant1->setImplication('Organisatrice');
        $participant1->addAtelier($this->getReference('atelier-1'));
        $participant1->addAtelier($this->getReference('atelier-2'));
        $manager->persist($participant1);
        
        // @var Participant
        $participant2 = new Participant();
        $participant2->setPseudo('Rachelle');
        $participant2->setEmail('rachelle@cooreparation-nantes.fr');
        $participant2->setImplication('Organisatrice');
        $participant2->addAtelier($this->getReference('atelier-1'));
        $participant2->addAtelier($this->getReference('atelier-2'));
        $manager->persist($participant2);
        
        // @var Participant
        $participant3 = new Participant();
        $participant3->setPseudo('Nicolas');
        $participant3->setEmail('nicolas@example.com');
        $participant3->setImplication('Documentaliste');
        $participant3->addAtelier($this->getReference('atelier-1'));
        $manager->persist($participant3);
        
        // @var Participant
        $participant4 = new Participant();
        $participant4->setPseudo('Martin');
        $participant4->setEmail(null);
        $participant4->setImplication('Visiteur');
        $participant4->addAtelier($this->getReference('atelier-1'));
        $manager->persist($participant4);
        
        // @var Participant
        $participant5 = new Participant();
        $participant5->setPseudo('Dominique');
        $participant5->setEmail('dominique@example.com');
        $participant5->setImplication('Visiteur');
        $participant5->addAtelier($this->getReference('atelier-2'));
        $manager->persist($participant5);
        
        // @var Participant
        $participant6 = new Participant();
        $participant6->setPseudo('CHiPs44');
        $participant6->setEmail('chips44@gitlab.com');
        $participant6->setImplication('Développeur ;-)');
        $participant6->addAtelier($this->getReference('atelier-1'));
        $participant6->addAtelier($this->getReference('atelier-2'));
        $participant6->setUser($this->getReference('user-admin'));
        $manager->persist($participant6);
        
        $manager->flush();
        
        $this->addReference('participant-accueil'       , $participant1);
        $this->addReference('participant-bricole'       , $participant2);
        $this->addReference('participant-documentaliste', $participant3);
        $this->addReference('participant-visiteur1'     , $participant4);
        $this->addReference('participant-visiteur2'     , $participant5);
        $this->addReference('participant-chips44'       , $participant6);
    }
}

// EOF
