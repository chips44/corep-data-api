<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Panne;

/**
 * Load data for entity "Panne"
 * 
 * @category DataFixture
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 */
class PanneFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * Get dependencies
     * 
     * @return array
     */
    public function getDependencies()
    {
        return array(
            OutilFixture::class,
            PieceFixture::class,
        );
    }

    /**
     * Load data
     * 
     * @param ObjectManager $manager Object manager
     * 
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        // @var Panne
        $panne1 = new Panne();
        $panne1->setObjet($this->getReference('objet-velo'));
        $panne1->setDescription('Mon vélo ne freine pas de l\'avant.');
        $panne1->setDiagnostic('Les patins sont usés, il faut les changer.');
        $panne1->setOperations(
            "1. démonter les vieux patins\r\n" .
            "2. monter les nouveaux\r\n" .
            "3. régler la tension du câble"
        );
        $panne1->addOutil($this->getReference('outil-pince-universelle'));
        $panne1->addOutil($this->getReference('outil-cle-plate'));
        $panne1->addPiece($this->getReference('piece-patin-de-frein'));
        $manager->persist($panne1);

        // @var Panne
        $panne2 = new Panne();
        $panne2->setObjet($this->getReference('objet-ordinateur'));
        $panne2->setDescription('Mon ordinateur rame et fait beaucoup de bruit.');
        $panne2->setDiagnostic('Il n\'a pas assez de mémoire et le ventilateur est sale.');
        $panne2->setOperations('Ajouter deux barettes de mémoire DDR2 1Go et nettoyer le ventilateur avec un pinceau.');
        $panne2->addOutil($this->getReference('outil-tournevis-cruciforme'));
        $panne2->addOutil($this->getReference('outil-pinceau'));
        $panne2->addPiece($this->getReference('piece-barette-memoire-ddr2-1go'));
        $manager->persist($panne2);

        $manager->flush();
    }

}

// EOF
