# To empty database

## Delete data

```sql
delete from pannes_outils;
delete from pieces_poles; 
delete from pannes_pieces;
delete from outils_poles;
delete from participants_ateliers;
delete from ateliers_poles;
delete from panne;
delete from objet;
delete from piece;
delete from outil;
delete from participant;
delete from atelier;
delete from pole;
delete from api_user;
```

## Drop tables:

```sql
drop table pannes_outils;
drop table pieces_poles; 
drop table pannes_pieces;
drop table outils_poles;
drop table participants_ateliers;
drop table ateliers_poles;
drop table panne;
drop table objet;
drop table piece;
drop table outil;
drop table participant;
drop table atelier;
drop table pole;
drop table api_user;
drop table refresh_tokens;
```

## Drop sequences:

```sql
drop sequence api_user_id_seq;
drop sequence atelier_id_seq;
drop sequence objet_id_seq;
drop sequence outil_id_seq;
drop sequence panne_id_seq;
drop sequence participant_id_seq;
drop sequence piece_id_seq;
drop sequence pole_id_seq;
drop sequence refresh_tokens_id_seq;
```

_That's all, folks!_
