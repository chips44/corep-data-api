<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Piece;

/**
 * Load data for entity "Piece" 
 * 
 * @category DataFixture
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 */
class PieceFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * Get dependencies
     * 
     * @return array
     */
    public function getDependencies()
    {
        return array(
            PoleFixture::class,
        );
    }
    
    /**
     * Load data
     * 
     * @param ObjectManager $manager Object manager
     * 
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        // @var Piece
        $piece1 = new Piece();
        $piece1->setNom('Vis tête fendue');
        $piece1->addPole($this->getReference('pole-mecanique'));
        $piece1->addPole($this->getReference('pole-velo'));
        $piece1->addPole($this->getReference('pole-auto'));
        $piece1->addPole($this->getReference('pole-electro-menager'));
        $manager->persist($piece1);
        
        // @var Piece
        $piece2 = new Piece();
        $piece2->setNom('Vis cruciforme');
        $piece2->addPole($this->getReference('pole-mecanique'));
        $piece2->addPole($this->getReference('pole-velo'));
        $piece2->addPole($this->getReference('pole-auto'));
        $piece2->addPole($this->getReference('pole-electro-menager'));
        $piece2->addPole($this->getReference('pole-informatique'));
        $manager->persist($piece2);
        
        // @var Piece
        $piece3 = new Piece();
        $piece3->setNom('Fusible');
        $piece3->addPole($this->getReference('pole-electro-menager'));
        $piece3->addPole($this->getReference('pole-informatique'));
        $manager->persist($piece3);
        
        // @var Piece
        $piece4 = new Piece();
        $piece4->setNom('Fil');
        $piece4->addPole($this->getReference('pole-couture'));
        $manager->persist($piece4);
        
        // @var Piece
        $piece5 = new Piece();
        $piece5->setNom('Patin de frein');
        $piece5->addPole($this->getReference('pole-velo'));
        $manager->persist($piece5);
        
        // @var Piece
        $piece6 = new Piece();
        $piece6->setNom('Barette de mémoire DDR2 1 Go');
        $piece6->addPole($this->getReference('pole-informatique'));
        $manager->persist($piece6);

        // @var Piece
        $piece7 = new Piece();
        $piece7->setNom('Barette de mémoire DDR3 4 Go');
        $piece7->addPole($this->getReference('pole-informatique'));
        $manager->persist($piece6);

        $manager->flush();
        
        $this->addReference('piece-vis-tete-fendue'         , $piece1);
        $this->addReference('piece-vis-cruciforme'          , $piece2);
        $this->addReference('piece-clou'                    , $piece3);
        $this->addReference('piece-fil'                     , $piece4);
        $this->addReference('piece-patin-de-frein'          , $piece5);
        $this->addReference('piece-barette-memoire-ddr2-1go', $piece6);
        $this->addReference('piece-barette-memoire-ddr3-4go', $piece7);

    }
}

// EOF
