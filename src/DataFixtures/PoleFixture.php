<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Pole;

/**
 * Load data for entity "Pole"
 * 
 * @category DataFixture
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 */
class PoleFixture extends Fixture
{
    /**
     * Load data
     * 
     * @param ObjectManager $manager Object manager
     * 
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        //echo "PoleFixture::BEGIN\n";

        // @var Pole
        $pole1 = new Pole();
        $pole1->setNom('Mécanique');
        $pole1->setParent(null);
        $manager->persist($pole1);
        
        // @var Pole
        $pole2 = new Pole();
        $pole2->setNom('Couture');
        $pole2->setParent(null);
        $manager->persist($pole2);
        
        // @var Pole
        $pole3 = new Pole();
        $pole3->setNom('Électro-ménager');
        $pole3->setParent(null);
        $manager->persist($pole3);
        
        // @var Pole
        $pole4 = new Pole();
        $pole4->setNom('Informatique');
        $pole4->setParent(null);
        $manager->persist($pole4);
        
        // @var Pole
        $pole5 = new Pole();
        $pole5->setNom('Vélo');
        $pole5->setParent($pole1);
        $manager->persist($pole5);
        
        // @var Pole
        $pole6 = new Pole();
        $pole6->setNom('Auto');
        $pole6->setParent($pole1);
        $manager->persist($pole6);
        
        $manager->flush();
        
        $this->addReference('pole-mecanique'      , $pole1);
        $this->addReference('pole-couture'        , $pole2);
        $this->addReference('pole-electro-menager', $pole3);
        $this->addReference('pole-informatique'   , $pole4);
        $this->addReference('pole-velo'           , $pole5);
        $this->addReference('pole-auto'           , $pole6);

        //echo "PoleFixture::END\n";
    }
}

// EOF
