<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * Load data for entity "User"
 * 
 * @category DataFixture
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 */
class UserFixture extends Fixture
{
    // cf. https://github.com/liip/LiipImagineBundle/issues/1033
    private $_securityEncoder;

    /**
     * Constructeur
     * 
     * @param EncoderFactoryInterface $securityEncoder Security encoder
     */
    public function __construct(EncoderFactoryInterface $securityEncoder) {
        $this->_securityEncoder = $securityEncoder;
    }
    
    /**
     * Load data
     * 
     * @param ObjectManager $manager Object manager
     * 
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        // @var User
        $admin = new User();
        $admin->setUsername('admin');
        $encoder = $this->_securityEncoder->getEncoder('App\Entity\User');
        $encodedPassword = $encoder->encodePassword('password', $admin->getSalt());
        $admin->setPassword($encodedPassword);
        $admin->setEmail('admin@example.com');
        $admin->setIsActive(true);
        $manager->persist($admin);

        // @var User
        $user = new User();
        $user->setUsername('username');
        $encoder = $this->_securityEncoder->getEncoder('App\Entity\User');
        $encodedPassword = $encoder->encodePassword('password', $user->getSalt());
        $user->setPassword($encodedPassword);
        $user->setEmail('username@example.com');
        $user->setIsActive(true);
        $manager->persist($user);

        $manager->flush();

        $this->addReference('user-admin', $admin);
        $this->addReference('user-user' , $user);
    }
}

// EOF
