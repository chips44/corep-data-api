<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Outil;

/**
 * Load data for entity "Outil"
 * 
 * @category DataFixture
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 */
class OutilFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * Get dependencies
     * 
     * @return array
     */
    public function getDependencies()
    {
        return array(
            PoleFixture::class,
        );
    }
    
    /**
     * Load data
     * 
     * @param ObjectManager $manager Object manager
     * 
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        // @var Outil
        $outil1 = new Outil();
        $outil1->setNom('Tournevis plat');
        $outil1->addPole($this->getReference('pole-mecanique'));
        $outil1->addPole($this->getReference('pole-velo'));
        $outil1->addPole($this->getReference('pole-auto'));
        $outil1->addPole($this->getReference('pole-electro-menager'));
        $manager->persist($outil1);
        
        // @var Outil
        $outil2 = new Outil();
        $outil2->setNom('Tournevis cruciforme');
        $outil2->addPole($this->getReference('pole-mecanique'));
        $outil2->addPole($this->getReference('pole-velo'));
        $outil2->addPole($this->getReference('pole-auto'));
        $outil2->addPole($this->getReference('pole-electro-menager'));
        $outil2->addPole($this->getReference('pole-informatique'));
        $manager->persist($outil2);
        
        // @var Outil
        $outil3 = new Outil();
        $outil3->setNom('Pince universelle');
        $outil3->addPole($this->getReference('pole-mecanique'));
        $outil3->addPole($this->getReference('pole-velo'));
        $outil3->addPole($this->getReference('pole-auto'));
        $manager->persist($outil3);
        
        // @var Outil
        $outil4 = new Outil();
        $outil4->setNom('Machine à coudre');
        $outil4->addPole($this->getReference('pole-couture'));
        $manager->persist($outil4);
        
        // @var Outil
        $outil5 = new Outil();
        $outil5->setNom('Clé plate');
        $outil5->addPole($this->getReference('pole-mecanique'));
        $outil5->addPole($this->getReference('pole-velo'));
        $outil5->addPole($this->getReference('pole-auto'));
        $outil5->addPole($this->getReference('pole-electro-menager'));
        $manager->persist($outil5);
        
        // @var Outil
        $outil6 = new Outil();
        $outil6->setNom('Pinceau');
        $outil6->addPole($this->getReference('pole-electro-menager'));
        $outil6->addPole($this->getReference('pole-informatique'));
        $manager->persist($outil6);

        $manager->flush();
        
        $this->addReference('outil-tournevis-plat'      , $outil1);
        $this->addReference('outil-tournevis-cruciforme', $outil2);
        $this->addReference('outil-pince-universelle'   , $outil3);
        $this->addReference('outil-machine-a-coudre'    , $outil4);
        $this->addReference('outil-cle-plate'           , $outil5);
        $this->addReference('outil-pinceau'             , $outil6);
    }
}

// EOF
