<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Atelier;

/**
 * Load data for entity "Atelier"
 * 
 * @category DataFixture
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 */
class AtelierFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * Get dependencies
     * 
     * @return array
     */
    public function getDependencies()
    {
        //echo "Atelier::getDependencies\n";
        return array(
            PoleFixture::class,
        );
    }

    /**
     * Load data
     * 
     * @param ObjectManager $manager Object manager
     * 
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        // @var Atelier
        $atelier1 = new Atelier();
        $atelier1->setLieu('Bellamy 17');
        $atelier1->setDate(new \DateTime('2017-04-22'));
        $atelier1->setTitre("Atelier de co-réparation du 22 avril 2017 de 10h à 17h à Bellamy 17");
        $atelier1->setNotes(
            "Avec la participation de l'Atelier et de Linux-Nantes, " . 
            "et sur les pôles Vélo, Couture, Électro-ménager, Informatique."
        );
        $atelier1->addPole($this->getReference('pole-velo'));
        $atelier1->addPole($this->getReference('pole-couture'));
        $atelier1->addPole($this->getReference('pole-electro-menager'));
        $atelier1->addPole($this->getReference('pole-informatique'));
        $manager->persist($atelier1);        
       
        // @var Atelier
        $atelier2 = new Atelier();
        $atelier2->setLieu('Quelque part');
        $atelier2->setDate(new \DateTime('2016-12-31'));
        $atelier2->setTitre("Atelier de co-réparation...");
        $atelier2->setNotes(
            "Histoire d'avoir 2 ateliers dans la base de données !"
        );
        $atelier2->addPole($this->getReference('pole-mecanique'));
        $manager->persist($atelier2);        

        $manager->flush();
        
        $this->addReference('atelier-1', $atelier1);
        $this->addReference('atelier-2', $atelier2);
    }
}

// EOF
