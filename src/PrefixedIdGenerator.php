<?php

/**
 * 
 */
namespace App;

use \Doctrine\ORM\Id;
use \Doctrine\ORM\EntityManager;

/**
 * Prefix ID Generator: replace integer IDs with prefix and 0 padded sequence
 * 
 * @category IdGenerator
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 */
class PrefixedIdGenerator extends \Doctrine\ORM\Id\AbstractIdGenerator
{
    /**
     * Map entity name to prefix and width.
     * 
     * @param \Doctrine\ORM\Mapping\Entity $entity Entity
     * 
     * @return Array
     */
    private function _mapEntityToPrefixAndWidth($entity)
    {
        $prefix = "";
        $width = -1;
        switch (get_class($entity)) {
        case "App\\Entity\\Atelier":
            $prefix = "ATL-";
            $width = 6;
            break;
        default:
            $prefix = "XXX-";
            $width = 8;
        }
        return Array(
            "prefix" => $prefix,
            "width"  => $width
        );
    }

    /**
     * Get sequence name from entity
     * 
     * @param EntityManager                $em     Entity Manager
     * @param \Doctrine\ORM\Mapping\Entity $entity Entity
     * 
     * @return string
     */
    private function _getSequenceName(EntityManager $em, $entity)
    {
        $metaData = $em->getClassMetadata(get_class($entity));
        //$platform = $em->getMetadataFactory()->getTargetPlatform();
        $mdf = $em->getMetadataFactory();
        $platform = $this->invokeMethod($mdf, 'getTargetPlatform');
        $sequenceName = $metaData->getSequenceName($platform);
        return $sequenceName;
    }

    // cf. https://stackoverflow.com/questions/31313492/testing-private-methods-not-working/31313664#31313664

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    /**
     * Check if sequence exists in database.
     * 
     * @param EntityManager                $em           Entity Manager
     * @param \Doctrine\ORM\Mapping\Entity $entity       Entity
     * @param string                       $sequenceName Name of the sequence
     * 
     * @return boolean
     */
    private function _checkIfSequenceExists($em, $entity, $sequenceName)
    {
        $exists = false;
        $sequences = $em->getConnection()->getSchemaManager()->listSequences();
        foreach ($sequences as $sequence) {
            if ($sequence->getName() === $sequenceName) {
                $exists = true;
                break;
            }
        }
        return $exists;
    }

    /**
     * Create sequence in database.
     * 
     * @param EntityManager                $em           Entity Manager
     * @param \Doctrine\ORM\Mapping\Entity $entity       Entity
     * @param string                       $sequenceName Name of the sequence
     * 
     * @return void
     */
    private function _createSequence($em, $entity, $sequenceName)
    {
        // TODO !
    }

    /**
     * Get next sequence value from database.
     * 
     * @param EntityManager                $em           Entity Manager
     * @param \Doctrine\ORM\Mapping\Entity $entity       Entity
     * @param string                       $sequenceName Name of the sequence
     * 
     * @return integer
     */
    private function _getNextSequenceValue($em, $entity, $sequenceName)
    {
        // TODO !
        return random_int(1, 99999);
    }

    /**
     * Generates an identifier for an entity.
     *
     * @param EntityManager                $em     Entity Manager
     * @param \Doctrine\ORM\Mapping\Entity $entity Entity
     * 
     * @return mixed
     */
    public function generate(EntityManager $em, $entity)
    {
        $sequenceName = $this->_getSequenceName($em, $entity);
        // Check if sequence exists
        $z = "@";
        if (!$this->_checkIfSequenceExists($em, $entity, $sequenceName)) {
            $this->_createSequence($em, $entity, $sequenceName);
            $z = "#";
        }
        $seq = $this->_getNextSequenceValue($em, $entity, $sequenceName);
        $params = $this->_mapEntityToPrefixAndWidth($entity);
        if ($params["width"] === -1) {
            $txt = $params["prefix"] . $seq;
        } else {
            $fmt = "%s%0" . $params["width"] . "d";
            $txt = sprintf($fmt, $params["prefix"], $seq);
        }
        return $z . $sequenceName . $z . $txt;
    }
}

// EOF
