<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181110164250 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE outil_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE participant_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE panne_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE atelier_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE pole_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE _user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE piece_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE objet_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE refresh_tokens_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE outil (id INT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE outils_poles (outil_id INT NOT NULL, pole_id INT NOT NULL, PRIMARY KEY(outil_id, pole_id))');
        $this->addSql('CREATE INDEX IDX_CC496603ED89C80 ON outils_poles (outil_id)');
        $this->addSql('CREATE INDEX IDX_CC49660419C3385 ON outils_poles (pole_id)');
        $this->addSql('CREATE TABLE participant (id INT NOT NULL, api_user_id INT DEFAULT NULL, pseudo VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, implication VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D79F6B1186CC499D ON participant (pseudo)');
        $this->addSql('CREATE INDEX IDX_D79F6B114A50A7F2 ON participant (api_user_id)');
        $this->addSql('CREATE TABLE participants_ateliers (participant_id INT NOT NULL, atelier_id INT NOT NULL, PRIMARY KEY(participant_id, atelier_id))');
        $this->addSql('CREATE INDEX IDX_12B6C7B9D1C3019 ON participants_ateliers (participant_id)');
        $this->addSql('CREATE INDEX IDX_12B6C7B82E2CF35 ON participants_ateliers (atelier_id)');
        $this->addSql('CREATE TABLE panne (id INT NOT NULL, description TEXT NOT NULL, diagnostic TEXT NOT NULL, operations TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE pannes_outils (panne_id INT NOT NULL, outil_id INT NOT NULL, PRIMARY KEY(panne_id, outil_id))');
        $this->addSql('CREATE INDEX IDX_41C2492365B7B5BD ON pannes_outils (panne_id)');
        $this->addSql('CREATE INDEX IDX_41C249233ED89C80 ON pannes_outils (outil_id)');
        $this->addSql('CREATE TABLE pannes_pieces (panne_id INT NOT NULL, piece_id INT NOT NULL, PRIMARY KEY(panne_id, piece_id))');
        $this->addSql('CREATE INDEX IDX_22A28D8B65B7B5BD ON pannes_pieces (panne_id)');
        $this->addSql('CREATE INDEX IDX_22A28D8BC40FCFA8 ON pannes_pieces (piece_id)');
        $this->addSql('CREATE TABLE atelier (id INT NOT NULL, lieu VARCHAR(255) NOT NULL, date DATE NOT NULL, titre VARCHAR(255) NOT NULL, notes TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE ateliers_poles (atelier_id INT NOT NULL, pole_id INT NOT NULL, PRIMARY KEY(atelier_id, pole_id))');
        $this->addSql('CREATE INDEX IDX_AFEC99FF82E2CF35 ON ateliers_poles (atelier_id)');
        $this->addSql('CREATE INDEX IDX_AFEC99FF419C3385 ON ateliers_poles (pole_id)');
        $this->addSql('CREATE TABLE pole (id INT NOT NULL, parent_pole_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FD6042E133D447C3 ON pole (parent_pole_id)');
        $this->addSql('CREATE TABLE _user (id INT NOT NULL, username VARCHAR(254) NOT NULL, password VARCHAR(64) NOT NULL, email VARCHAR(254) NOT NULL, is_active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D0B6A652F85E0677 ON _user (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D0B6A652E7927C74 ON _user (email)');
        $this->addSql('CREATE TABLE piece (id INT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE pieces_poles (piece_id INT NOT NULL, pole_id INT NOT NULL, PRIMARY KEY(piece_id, pole_id))');
        $this->addSql('CREATE INDEX IDX_94A9C982C40FCFA8 ON pieces_poles (piece_id)');
        $this->addSql('CREATE INDEX IDX_94A9C982419C3385 ON pieces_poles (pole_id)');
        $this->addSql('CREATE TABLE objet (id INT NOT NULL, pole_id INT DEFAULT NULL, accueillant_id INT DEFAULT NULL, visiteur_id INT DEFAULT NULL, bricoleur_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_46CD4C38419C3385 ON objet (pole_id)');
        $this->addSql('CREATE INDEX IDX_46CD4C383742B4D5 ON objet (accueillant_id)');
        $this->addSql('CREATE INDEX IDX_46CD4C387F72333D ON objet (visiteur_id)');
        $this->addSql('CREATE INDEX IDX_46CD4C38250E5CF4 ON objet (bricoleur_id)');
        $this->addSql('CREATE TABLE refresh_tokens (id INT NOT NULL, refresh_token VARCHAR(128) NOT NULL, username VARCHAR(255) NOT NULL, valid TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9BACE7E1C74F2195 ON refresh_tokens (refresh_token)');
        $this->addSql('ALTER TABLE outils_poles ADD CONSTRAINT FK_CC496603ED89C80 FOREIGN KEY (outil_id) REFERENCES outil (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE outils_poles ADD CONSTRAINT FK_CC49660419C3385 FOREIGN KEY (pole_id) REFERENCES pole (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE participant ADD CONSTRAINT FK_D79F6B114A50A7F2 FOREIGN KEY (api_user_id) REFERENCES _user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE participants_ateliers ADD CONSTRAINT FK_12B6C7B9D1C3019 FOREIGN KEY (participant_id) REFERENCES participant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE participants_ateliers ADD CONSTRAINT FK_12B6C7B82E2CF35 FOREIGN KEY (atelier_id) REFERENCES atelier (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pannes_outils ADD CONSTRAINT FK_41C2492365B7B5BD FOREIGN KEY (panne_id) REFERENCES panne (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pannes_outils ADD CONSTRAINT FK_41C249233ED89C80 FOREIGN KEY (outil_id) REFERENCES outil (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pannes_pieces ADD CONSTRAINT FK_22A28D8B65B7B5BD FOREIGN KEY (panne_id) REFERENCES panne (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pannes_pieces ADD CONSTRAINT FK_22A28D8BC40FCFA8 FOREIGN KEY (piece_id) REFERENCES piece (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ateliers_poles ADD CONSTRAINT FK_AFEC99FF82E2CF35 FOREIGN KEY (atelier_id) REFERENCES atelier (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ateliers_poles ADD CONSTRAINT FK_AFEC99FF419C3385 FOREIGN KEY (pole_id) REFERENCES pole (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pole ADD CONSTRAINT FK_FD6042E133D447C3 FOREIGN KEY (parent_pole_id) REFERENCES pole (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pieces_poles ADD CONSTRAINT FK_94A9C982C40FCFA8 FOREIGN KEY (piece_id) REFERENCES piece (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE pieces_poles ADD CONSTRAINT FK_94A9C982419C3385 FOREIGN KEY (pole_id) REFERENCES pole (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE objet ADD CONSTRAINT FK_46CD4C38419C3385 FOREIGN KEY (pole_id) REFERENCES pole (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE objet ADD CONSTRAINT FK_46CD4C383742B4D5 FOREIGN KEY (accueillant_id) REFERENCES participant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE objet ADD CONSTRAINT FK_46CD4C387F72333D FOREIGN KEY (visiteur_id) REFERENCES participant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE objet ADD CONSTRAINT FK_46CD4C38250E5CF4 FOREIGN KEY (bricoleur_id) REFERENCES participant (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE outils_poles DROP CONSTRAINT FK_CC496603ED89C80');
        $this->addSql('ALTER TABLE pannes_outils DROP CONSTRAINT FK_41C249233ED89C80');
        $this->addSql('ALTER TABLE participants_ateliers DROP CONSTRAINT FK_12B6C7B9D1C3019');
        $this->addSql('ALTER TABLE objet DROP CONSTRAINT FK_46CD4C383742B4D5');
        $this->addSql('ALTER TABLE objet DROP CONSTRAINT FK_46CD4C387F72333D');
        $this->addSql('ALTER TABLE objet DROP CONSTRAINT FK_46CD4C38250E5CF4');
        $this->addSql('ALTER TABLE pannes_outils DROP CONSTRAINT FK_41C2492365B7B5BD');
        $this->addSql('ALTER TABLE pannes_pieces DROP CONSTRAINT FK_22A28D8B65B7B5BD');
        $this->addSql('ALTER TABLE participants_ateliers DROP CONSTRAINT FK_12B6C7B82E2CF35');
        $this->addSql('ALTER TABLE ateliers_poles DROP CONSTRAINT FK_AFEC99FF82E2CF35');
        $this->addSql('ALTER TABLE outils_poles DROP CONSTRAINT FK_CC49660419C3385');
        $this->addSql('ALTER TABLE ateliers_poles DROP CONSTRAINT FK_AFEC99FF419C3385');
        $this->addSql('ALTER TABLE pole DROP CONSTRAINT FK_FD6042E133D447C3');
        $this->addSql('ALTER TABLE pieces_poles DROP CONSTRAINT FK_94A9C982419C3385');
        $this->addSql('ALTER TABLE objet DROP CONSTRAINT FK_46CD4C38419C3385');
        $this->addSql('ALTER TABLE participant DROP CONSTRAINT FK_D79F6B114A50A7F2');
        $this->addSql('ALTER TABLE pannes_pieces DROP CONSTRAINT FK_22A28D8BC40FCFA8');
        $this->addSql('ALTER TABLE pieces_poles DROP CONSTRAINT FK_94A9C982C40FCFA8');
        $this->addSql('DROP SEQUENCE outil_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE participant_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE panne_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE atelier_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE pole_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE _user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE piece_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE objet_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE refresh_tokens_id_seq CASCADE');
        $this->addSql('DROP TABLE outil');
        $this->addSql('DROP TABLE outils_poles');
        $this->addSql('DROP TABLE participant');
        $this->addSql('DROP TABLE participants_ateliers');
        $this->addSql('DROP TABLE panne');
        $this->addSql('DROP TABLE pannes_outils');
        $this->addSql('DROP TABLE pannes_pieces');
        $this->addSql('DROP TABLE atelier');
        $this->addSql('DROP TABLE ateliers_poles');
        $this->addSql('DROP TABLE pole');
        $this->addSql('DROP TABLE _user');
        $this->addSql('DROP TABLE piece');
        $this->addSql('DROP TABLE pieces_poles');
        $this->addSql('DROP TABLE objet');
        $this->addSql('DROP TABLE refresh_tokens');
    }
}
