<?php

declare(strict_types=1);

namespace App\PathSegmentNameGenerator;

use ApiPlatform\Core\Operation\PathSegmentNameGeneratorInterface;
use Doctrine\Common\Inflector\Inflector;

/**
 * Description of MyPathSegmentNameGenerator
 *
 * @author CHiPs44 <chips44@gitlab.com>
 */
final class MyPathSegmentNameGenerator implements PathSegmentNameGeneratorInterface
{
    /**
     * Get segment name
     * 
     * @param string $name       name
     * @param bool   $collection true if collection
     *
     * @return string Segment name
     */
    public function getSegmentName(string $name, bool $collection = true): string
    {
        if ($collection) {
            $path = Inflector::pluralize(strtolower($name));
        } else {
            $path = strtolower($name);
        }
        return $path;
    }
}

// EOF
