<?php

declare(strict_types=1);

namespace App\OperationPathResolver;

use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\Api\OperationTypeDeprecationHelper;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use ApiPlatform\Core\PathResolver\OperationPathResolverInterface;
use ApiPlatform\Core\Operation\PathSegmentNameGeneratorInterface;
use Symfony\Flex\Unpack\Operation;

/**
 * Generates an operation path.
 *
 * @author CHiPs44 <chips44@gitlab.com>
 */
final class MyOperationPathResolver implements OperationPathResolverInterface
{
    private $_pathSegmentNameGenerator;

    /**
     * Constructor
     * 
     * @param PathSegmentNameGeneratorInterface $pathSegmentNameGenerator Path segment name generator
     */
    public function __construct(PathSegmentNameGeneratorInterface $pathSegmentNameGenerator)
    {
        $this->_pathSegmentNameGenerator = $pathSegmentNameGenerator;
    }

    /**
     * Resolves the operation path.
     *
     * @param string      $resourceShortName When the operation type is a subresource and the operation has more than one identifier, this value is the previous operation path
     * @param array       $operation         The operation metadata
     * @param string|bool $operationType     One of the constants defined in ApiPlatform\Core\Api\OperationType
     *                                       If the property is a boolean, true represents OperationType::COLLECTION, false is for OperationType::ITEM
     * 
     * @return string
     */
    public function resolveOperationPath(string $resourceShortName, array $operation, $operationType/*, string $operationName = null*/): string
    {
        if (\func_num_args() < 4) {
            $message = sprintf(
                "Method %s() will have a 4th `string \$operationName` argument in version 3.0. " . 
                "Not defining it is deprecated since 2.1.", 
                METHOD
            );
            @trigger_error($message, E_USER_DEPRECATED);
        }
        $operationType = OperationTypeDeprecationHelper::getOperationType(
            $operationType
        );
        if (OperationType::SUBRESOURCE === $operationType) {
            throw new InvalidArgumentException(
                'Subresource operations are not supported by the OperationPathResolver.'
            );
        }
        $collection = OperationType::COLLECTION == $operationType ? true : false;
        $path = '/' . $this->_pathSegmentNameGenerator->getSegmentName(
            $resourceShortName, 
            $collection
        );
        if (OperationType::ITEM === $operationType) {
            $path .= '/{id}';
        }

        $path .= '{_format}';
        //die("resolveOperationPath: path=$path");
        return $path;
    }
}

// EOF

