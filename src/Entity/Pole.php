<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Pôle
 *   1er niveau : Mécanique, Couture, Informatique, Électro-ménager, ...
 *   autres niveaux : voir si intéressant ? le parent est là.
 *
 * @category Entity
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 * 
 * @ApiResource
 * @ORM\Table(name="pole")
 * @ORM\Entity(repositoryClass="App\Repository\PoleRepository")
 */
class Pole
{
    /**
     * ID du pôle
     * 
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Nom du pôle
     * 
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;
    
    /**
     * Parent du pôle
     * 
     * @ORM\ManyToOne(targetEntity="Pole", inversedBy="enfants")
     * @ORM\JoinColumn(name="parent_pole_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * Enfants du pôle
     * 
     * @ORM\OneToMany(targetEntity="Pole", mappedBy="parent")
     */
    private $enfants;

    /**
     * Lire l'ID du pôle
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Écrire le nom du pôle
     *
     * @param string $nom Nom du pôle
     *
     * @return Pole
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Lire le nom du pôle
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->enfants = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Écrire le parent du pôle
     *
     * @param \App\Entity\Pole $parent Parent du pôle
     *
     * @return Pole
     */
    public function setParent(\App\Entity\Pole $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Lire le parent du pôle
     *
     * @return \App\Entity\Pole
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Ajouter un enfant au pôle
     *
     * @param \App\Entity\Pole $enfant Enfant à ajouter
     *
     * @return Pole
     */
    public function addEnfant(\App\Entity\Pole $enfant)
    {
        $this->enfants[] = $enfant;

        return $this;
    }

    /**
     * Enlever un enfant du pôle
     *
     * @param \App\Entity\Pole $enfant Enfant à supprimer
     * 
     * @return void
     */
    public function removeEnfant(\App\Entity\Pole $enfant)
    {
        $this->enfants->removeElement($enfant);
    }

    /**
     * Lire les enfants du pôle
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnfants()
    {
        return $this->enfants;
    }
}

// EOF
