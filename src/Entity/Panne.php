<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Panne
 *
 * @category Entity
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 * 
 * @ApiResource
 * @ORM\Table(name="panne")
 * @ORM\Entity(repositoryClass="App\Repository\PanneRepository")
 */
class Panne {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /*
     * @var Objet l'objet de la panne
     * 
     * @ORM\Column(name="objet_id", type="integer")
     * @ORM\ManyToOne(targetEntity="Objet")
     * @ORM\JoinColumn(name="objet_id", referencedColumnName="id")
     */
    private $objet;

    /**
     * @var string Description de la panne : 
     *             mon vélo ne freine pas de l'avant
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var string Diagnostic de la panne :
     *             les patins sont usés, il faut les changer
     *
     * @ORM\Column(name="diagnostic", type="text")
     */
    private $diagnostic;

    /**
     * @var string Opérations effectuées pour réparer :
     *             1. démonter les vieux patins
     *             2. monter les nouveaux
     *             3. régler la tension du câble
     *
     * @ORM\Column(name="operations", type="text")
     */
    private $operations;

    /**
     * @var Outil[] Outils utilisés pour réparer
     *      - Clé plate
     *      - Pince universelle
     * 
     * @ORM\ManyToMany(targetEntity="Outil")
     * @ORM\JoinTable(
     *      name="pannes_outils",
     *      joinColumns={
     *          @ORM\JoinColumn(name="panne_id", referencedColumnName="id")
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="outil_id", referencedColumnName="id")
     *      }
     *  )
     */
    private $outils;

    /**
     * @var Piece[] Pièces utilisées (ou à utiliser si diagnostic car pas disponibles)
     *      - Patin de frein x2
     * 
     * @ORM\ManyToMany(targetEntity="Piece")
     * @ORM\JoinTable(
     *      name="pannes_pieces",
     *      joinColumns={
     *          @ORM\JoinColumn(name="panne_id", referencedColumnName="id")
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="piece_id", referencedColumnName="id")
     *      }
     *  )
     */
    private $pieces;

    /**
     * Constructor
     */
    public function __construct() {
        $this->outils = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pieces = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set objet
     *
     * @param \App\Entity\Objet $objet
     *
     * @return Panne
     */
    public function setObjet(\App\Entity\Objet $objet) {
        $this->objet = $objet;

        return $this;
    }

    /**
     * Get objet
     *
     * @return \App\EntityaddOutil\Objet
     */
    public function getObjet() {
        return $this->objet;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Panne
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set diagnostic
     *
     * @param string $diagnostic
     *
     * @return Panne
     */
    public function setDiagnostic($diagnostic) {
        $this->diagnostic = $diagnostic;

        return $this;
    }

    /**
     * Get diagnostic
     *
     * @return string
     */
    public function getDiagnostic() {
        return $this->diagnostic;
    }

    /**
     * Set operations
     *
     * @param string $operations
     *
     * @return Panne
     */
    public function setOperations($operations) {
        $this->operations = $operations;

        return $this;
    }

    /**
     * Get operations
     *
     * @return string
     */
    public function getOperations() {
        return $this->operations;
    }

    /**
     * Add outil
     *
     * @param \App\Entity\Outil $outil
     *
     * @return Atelier
     */
    public function addOutil(\App\Entity\Outil $outil) {
        $this->outils[] = $outil;

        return $this;
    }

    /**
     * Remove outil
     *
     * @param \App\Entity\Outil $outil
     */
    public function removeOutil(\App\Entity\Outil $outil) {
        $this->outils->removeElement($outil);
    }

    /**
     * Get outils
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOutils() {
        return $this->outils;
    }

    /**
     * Add piece
     *
     * @param \App\Entity\Piece $piece
     *
     * @return Atelier
     */
    public function addPiece(\App\Entity\Piece $piece) {
        $this->pieces[] = $piece;

        return $this;
    }

    /**
     * Remove piece
     *
     * @param \App\Entity\Piece $piece
     */
    public function removePiece(\App\Entity\Piece $piece) {
        $this->pieces->removeElement($piece);
    }

    /**
     * Get pieces
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPieces() {
        return $this->pieces;
    }

}
