<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Outil (tournevis plat, tournevis cruciforme, clé plate, clé à pipe, 
 * pince universelle, fer à souder, dé à coudre, aiguille, épingles, 
 * ...)
 *
 * @category Entity
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 * 
 * @ApiResource
 * @ORM\Table(name="outil")
 * @ORM\Entity(repositoryClass="App\Repository\OutilRepository")
 */
class Outil
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;
    
    /**
     * @var Pole[]
     * 
     * @ORM\ManyToMany(targetEntity="Pole")
     * @ORM\JoinTable(
     *      name="outils_poles",
     *      joinColumns={
     *          @ORM\JoinColumn(name="outil_id", referencedColumnName="id")
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="pole_id", referencedColumnName="id")}
     *  )
     */
    private $poles;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Outil
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->poles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pole
     *
     * @param \App\Entity\Pole $pole
     *
     * @return Outil
     */
    public function addPole(\App\Entity\Pole $pole)
    {
        $this->poles[] = $pole;

        return $this;
    }

    /**
     * Remove pole
     *
     * @param \App\Entity\Pole $pole
     */
    public function removePole(\App\Entity\Pole $pole)
    {
        $this->poles->removeElement($pole);
    }

    /**
     * Get poles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPoles()
    {
        return $this->poles;
    }
}
