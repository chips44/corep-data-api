<?php

/**
 * Atelier
 * 
 * @category   Entity
 * @package    CorepDataApi
 * @author     CHiPs44 <chips44@gitlab.com>
 * @license    AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link       https://gitlab.com/chips44/corep-data-api
 * @phpversion 7.1
 */
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Atelier de cooréparation du 22 avril 2017 de 10h à 17h à Bellamy 17, 
 * avec la participation de l'Atelier et de Linux-Nantes, et sur les 
 * pôles Mécanique, Couture, Électro-ménager, Informatique, ...
 * 
 * @category Entity
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 * 
 * @ApiResource
 * @ORM\Table(name="atelier")
 * @ORM\Entity(repositoryClass="App\Repository\AtelierRepository")
 */
class Atelier
{
    /**
     * ID de l'atelier
     * 
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * _@_O_R_M_\GeneratedValue(strategy="CUSTOM")
     * _@_O_R_M_\CustomIdGenerator(class="App\PrefixedIdGenerator")
     */
    private $id;

    /**
     * Lieu dans lequel se déroule l'atelier :
     * "Bellamy 17"
     * 
     * @var string 
     *
     * @ORM\Column(name="lieu", type="string", length=255, nullable=false, unique=false)
     */
    private $lieu;

    /**
     * Date de l'atelier : 22/04/2017
     * 
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * Titre de l'atelier
     * "Atelier de co-réparation du 22 avril 2017 de 10h à 17h à Bellamy 17"
     * 
     * @var string
     *
     * @ORM\Column(name="titre", type="string")
     */
    private $titre;
    
    /**
     * Notes sur l'atelier
     * "Avec la participation de l'Atelier et de Linux-Nantes"
     * 
     * @var string 
     *
     * @ORM\Column(name="notes", type="text")
     */
    private $notes;
    
    /**
     * Pôles traités sur l'atelier
     * 
     * @var Pole[]
     * 
     * @ORM\ManyToMany(targetEntity="Pole")
     * @ORM\JoinTable(
     *      name="ateliers_poles",
     *      joinColumns={
     *          @ORM\JoinColumn(
     *              name="atelier_id",
     *              referencedColumnName="id"
     *          )
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="pole_id",
     *              referencedColumnName="id"
     *          )
     *      }
     *  )
     */
    private $poles;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lieu
     *
     * @param string $lieu Lieu de l'atelier
     *
     * @return Atelier
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set date
     *
     * @param \DateTime $date Date de l'atelier
     *
     * @return Atelier
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set titre
     *
     * @param string $titre Titre de l'atelier
     *
     * @return Atelier
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }
    
    /**
     * Set notes
     *
     * @param string $notes Notes sur l'atelier
     *
     * @return Atelier
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->poles = new ArrayCollection();
    }

    /**
     * Ajouter un pôle à l'atelier
     *
     * @param \App\Entity\Pole $pole Pôle à ajouter à l'atelier
     *
     * @return Atelier
     */
    public function addPole(\App\Entity\Pole $pole)
    {
        $this->poles[] = $pole;

        return $this;
    }

    /**
     * Supprimer un pôle de l'atelier
     *
     * @param \App\Entity\Pole $pole Pôle à supprimer de l'atelier
     * 
     * @return Atelier
     */
    public function removePole(\App\Entity\Pole $pole)
    {
        $this->poles->removeElement($pole);

        return $this;
    }

    /**
     * Récupérer la liste des pôles de l'atelier
     *
     * @return Collection
     */
    public function getPoles()
    {
        return $this->poles;
    }
}

// EOF
