<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Participant (Marie, marie@cooreparation-nantes.fr, Organisatrice, 
 * ou bien Martin, martin212@gmail.com, Visiteur)
 *
 * @category Entity
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 * 
 * @ApiResource
 * @ORM\Table(name="participant")
 * @ORM\Entity(repositoryClass="App\Repository\ParticipantRepository")
 */
class Participant
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pseudo", type="string", length=255, nullable=false, unique=true)
     */
    private $pseudo;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="implication", type="string", length=255, nullable=false)
     */
    private $implication;
    
    /**
     * @var Atelier[]
     * 
     * @ORM\ManyToMany(targetEntity="Atelier")
     * @ORM\JoinTable(
     *      name="participants_ateliers",
     *      joinColumns={
     *          @ORM\JoinColumn(name="participant_id", referencedColumnName="id")
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="atelier_id", referencedColumnName="id")
     *      }
     *  )
     */
    private $ateliers;

    /**
     * @var User
     * 
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="api_user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pseudo
     *
     * @param string $pseudo
     *
     * @return Participant
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * Get pseudo
     *
     * @return string
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Participant
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set implication
     *
     * @param string $implication
     *
     * @return Participant
     */
    public function setImplication($implication)
    {
        $this->implication = $implication;
        return $this;
    }

    /**
     * Get implication
     *
     * @return string
     */
    public function getImplication()
    {
        return $this->implication;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Participant
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ateliers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add atelier
     *
     * @param \App\Entity\Pole $atelier
     *
     * @return Participant
     */
    public function addAtelier(\App\Entity\Atelier $atelier)
    {
        $this->ateliers[] = $atelier;
        return $this;
    }

    /**
     * Remove atelier
     *
     * @param \App\Entity\Pole $atelier
     */
    public function removeAtelier(\App\Entity\Atelier $atelier)
    {
        $this->ateliers->removeElement($atelier);
    }

    /**
     * Get ateliers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAteliers()
    {
        return $this->ateliers;
    }
}
