<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Pièce (graisse, patin de frein, vis, écrou, fusible, soudure, 
 * barrette de mémoire, disque dur, fil, bouton, galon, ...)
 *
 * @category Entity
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 * 
 * @ApiResource
 * @ORM\Table(name="piece")
 * @ORM\Entity(repositoryClass="App\Repository\PieceRepository")
 */
class Piece
{
    /**
     * ID de la pièce
     * 
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Nom de la pièce
     * 
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;
    
    /**
     * Pôles associés à la pièce
     * 
     * @var Pole[]
     * 
     * @ORM\ManyToMany(targetEntity="Pole")
     * @ORM\JoinTable(
     *      name="pieces_poles",
     *      joinColumns={
     *          @ORM\JoinColumn(name="piece_id", referencedColumnName="id")
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="pole_id", referencedColumnName="id")}
     *  )
     */
    private $poles;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom Nom du pôle
     *
     * @return Piece
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->poles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pole
     *
     * @param \App\Entity\Pole $pole Pôle à ajouter
     *
     * @return Piece
     */
    public function addPole(\App\Entity\Pole $pole)
    {
        $this->poles[] = $pole;

        return $this;
    }

    /**
     * Remove pole
     *
     * @param \App\Entity\Pole $pole Pôle à enlever
     * 
     * @return void
     */
    public function removePole(\App\Entity\Pole $pole)
    {
        $this->poles->removeElement($pole);
    }

    /**
     * Get poles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPoles()
    {
        return $this->poles;
    }
}
