<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Objet
 *
 * @category Entity
 * @package  CorepDataApi
 * @author   CHiPs44 <chips44@gitlab.com>
 * @license  AGPL3+ https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @link     https://gitlab.com/chips44/corep-data-api
 * 
 * @ApiResource
 * @ORM\Table(name="objet")
 * @ORM\Entity(repositoryClass="App\Repository\ObjetRepository")
 */
class Objet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string le nom de l'objet
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var Pole le pôle dont fait partie l'objet (un seul)
     * 
     * @ORM\ManyToOne(targetEntity="Pole")
     * @ORM\JoinColumn(name="pole_id", referencedColumnName="id")
     */
    private $pole;
    
    /**
     * @var Participant le participant qui s'occupe de l'accueil
     * 
     * @ORM\ManyToOne(targetEntity="Participant")
     * @ORM\JoinColumn(name="accueillant_id", referencedColumnName="id")
     */
    private $accueillant;
    
    /**
     * @var Participant le participant qui vient pour la réparation
     * 
     * @ORM\ManyToOne(targetEntity="Participant")
     * @ORM\JoinColumn(name="visiteur_id", referencedColumnName="id")
     */
    private $visiteur;
    
    /**
     * @var Participant le participant qui aide à faire la réparation
     * 
     * @ORM\ManyToOne(targetEntity="Participant")
     * @ORM\JoinColumn(name="bricoleur_id", referencedColumnName="id")
     */
    private $bricoleur;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Objet
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set pole
     *
     * @param \App\Entity\Pole $pole
     *
     * @return Objet
     */
    public function setPole(\App\Entity\Pole $pole = null)
    {
        $this->pole = $pole;

        return $this;
    }

    /**
     * Get pole
     *
     * @return \App\Entity\Pole
     */
    public function getPole()
    {
        return $this->pole;
    }

    /**
     * Set accueillant
     *
     * @param \App\Entity\Participant $accueillant
     *
     * @return Objet
     */
    public function setAccueillant(\App\Entity\Participant $accueillant = null)
    {
        $this->accueillant = $accueillant;

        return $this;
    }

    /**
     * Get accueillant
     *
     * @return \App\Entity\Participant
     */
    public function getAccueillant()
    {
        return $this->accueillant;
    }

    /**
     * Set visiteur
     *
     * @param \App\Entity\Participant $visiteur
     *
     * @return Objet
     */
    public function setVisiteur(\App\Entity\Participant $visiteur = null)
    {
        $this->visiteur = $visiteur;

        return $this;
    }

    /**
     * Get visiteur
     *
     * @return \App\Entity\Participant
     */
    public function getVisiteur()
    {
        return $this->visiteur;
    }

    /**
     * Set bricoleur
     *
     * @param \App\Entity\Participant $bricoleur
     *
     * @return Objet
     */
    public function setBricoleur(\App\Entity\Participant $bricoleur = null)
    {
        $this->bricoleur = $bricoleur;

        return $this;
    }

    /**
     * Get bricoleur
     *
     * @return \App\Entity\Participant
     */
    public function getBricoleur()
    {
        return $this->bricoleur;
    }
}
