#!/bin/bash

ADD_JWT_TO_DOTENV=1

JWT_PRIVATE_KEY_PATH=config/jwt/private.pem
JWT_PUBLIC_KEY_PATH=config/jwt/public.pem

# Find JWT_PASSPHRASE in .env?
JWT_PASSPHRASE=$(grep "^JWT_PASSPHRASE=" .env | awk -F "=" '{printf "%s", $2}')

if [ "x$JWT_PASSPHRASE" == "x" ]; then
    # Generate passphrase with pwgen
    JWT_PASSPHRASE=$(pwgen -cnvs 64 1)
    ADD_JWT_TO_DOTENV=1
    echo "New JWT_PASSPHRASE generated with pwgen"
fi

if [ "$ADD_JWT_TO_DOTENV" == "1" ]; then
    echo "Adding JWT to .env"
    echo ""                                                         >> .env
    echo "###> lexik/jwt-authentication-bundle ###"                 >> .env
    echo "# Key paths should be relative to the project directory"  >> .env
    echo "JWT_PRIVATE_KEY_PATH=$JWT_PRIVATE_KEY_PATH"               >> .env
    echo "JWT_PUBLIC_KEY_PATH=$JWT_PUBLIC_KEY_PATH"                 >> .env
    echo "JWT_PASSPHRASE=$JWT_PASSPHRASE"                           >> .env
    echo "###< lexik/jwt-authentication-bundle ###"                 >> .env
    echo ""                                                         >> .env
    echo "Done."
fi

#echo "JWT_PASSPHRASE=$JWT_PASSPHRASE"
export JWT_PASSPHRASE
mkdir -p config/jwt
openssl genrsa -passout "env:JWT_PASSPHRASE" -out "$JWT_PRIVATE_KEY_PATH" -aes256 4096
openssl rsa -passin "env:JWT_PASSPHRASE" -pubout -in "$JWT_PRIVATE_KEY_PATH" -out "$JWT_PUBLIC_KEY_PATH"

# EOF
